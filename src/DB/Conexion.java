/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author gnr_a
 */
public class Conexion {
    Connection conect = null;
    
     public void conectar() {  
        String url = "jdbc:sqlserver://localhost:1433;database=Restaurant;user=test;password=test1";
         try {
             Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
             conect = DriverManager.getConnection(url);
             //System.out.println("Exito!!!");
             
         } catch (SQLException ex) {
             Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
             //System.out.println("Conexion falla");
         } catch (ClassNotFoundException ex) {
             Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
             //System.out.println("Clase no encontrada");
         }
    }
     
     public Connection getConexion(){
         return conect;
     }
     
     
}

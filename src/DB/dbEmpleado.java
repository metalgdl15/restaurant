/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DB;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import restaurant.Clases.Empleado;
import herramientas.Puesto;

/**
 *
 * @author gnr_a
 */
public class dbEmpleado {
    
    public void insert(Empleado empleado){
        Conexion conexion = new Conexion();
        conexion.conectar();        
       // byte[] foto = Files.readAllBytes(null);

        String sql= "INSERT INTO EMPLEADO(nombre,apellidoP,apellidoM, puesto, rfc, edad, foto, ingreso) VALUES (?,?,?,?,?,?,?,?)";
        try {
            
            PreparedStatement ps = conexion.getConexion().prepareStatement(sql);
            
            ps.setString(1, empleado.getNombre());
            ps.setString(2, empleado.getApellidoP());
            ps.setString(3, empleado.getApellidoM());
            ps.setString(4, empleado.getPuesto());
            ps.setString(5, empleado.getRFC());
            
            ps.setInt(6, empleado.getEdad());
 
            ps.setString(7, empleado.getFoto());
            ps.setString(8, empleado.getFechaIngreso());
           
            ps.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(dbEmpleado.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void delete(int codigo){
        Conexion conexion = new Conexion();
        conexion.conectar();        
       // byte[] foto = Files.readAllBytes(null);
        String sql= "DELETE FROM EMPLEADO WHERE codigo=?";
        try {
            
            PreparedStatement ps = conexion.getConexion().prepareStatement(sql);   
            ps.setInt(1, codigo);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(dbEmpleado.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public List<Empleado> read(){
        
        List<Empleado> lista = new ArrayList<>();
        
        Conexion conexion = new Conexion();
        conexion.conectar();
        
       // byte[] foto = Files.readAllBytes(null);
        String sql= "SELECT * FROM EMPLEADO";
        try {
            
            PreparedStatement ps = conexion.getConexion().prepareStatement(sql);   
            ResultSet rs = ps.executeQuery();
            
            while (rs.next()){
                Empleado empleado = new Empleado();
                empleado.setCodigo(rs.getInt(1));
                empleado.setNombre(rs.getString(2));
                empleado.setApellidoP(rs.getString(3));
                empleado.setApellidoM(rs.getString(4));
                empleado.setPuesto(rs.getString(5));              
                empleado.setRFC(rs.getString(8));
                empleado.setFoto(rs.getString(9));
                empleado.setEdad(rs.getInt(10));
                empleado.setFechaIngreso(rs.getString(11)); 
                
                lista.add(empleado);
            }
        } catch (SQLException ex) {
            Logger.getLogger(dbEmpleado.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return lista;
    }
    
    public Empleado readOnly(int codigo){
        Empleado empleado = new Empleado();
        
        Conexion conexion = new Conexion();
        conexion.conectar();        
       // byte[] foto = Files.readAllBytes(null);
        String sql= "SELECT * FROM EMPLEADO WHERE codigo=?";
        try {
            PreparedStatement ps = conexion.getConexion().prepareStatement(sql);
            ps.setInt(1, codigo);
            ResultSet rs = ps.executeQuery();

            while (rs.next()){
                empleado.setCodigo(rs.getInt(1));
                empleado.setNombre(rs.getString(2));
                empleado.setApellidoP(rs.getString(3));
                empleado.setApellidoM(rs.getString(4));
                
                //empleado.setPuesto(Puesto.valueOf(rs.getString(5)));
                empleado.setRFC(rs.getString(8));
                empleado.setEdad(rs.getInt(10));
                
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(dbEmpleado.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return empleado;
        
    }
    
      public List<Empleado> readSearch(String valor){
        
        List<Empleado> lista = new ArrayList<>();
        
        Conexion conexion = new Conexion();
        conexion.conectar();
        
       // byte[] foto = Files.readAllBytes(null);
        String sql= "SELECT * FROM EMPLEADO WHERE Nombre LIKE ?";
        try {
            
            PreparedStatement ps = conexion.getConexion().prepareStatement(sql);   
            ps.setString(1, valor + "%");
            
            ResultSet rs = ps.executeQuery();
            
            while (rs.next()){
                Empleado empleado = new Empleado();
                empleado.setCodigo(rs.getInt(1));
                empleado.setNombre(rs.getString(2));
                empleado.setApellidoP(rs.getString(3));
                empleado.setApellidoM(rs.getString(4));
                empleado.setPuesto(rs.getString(5));              
                empleado.setRFC(rs.getString(8));
                empleado.setFoto(rs.getString(9));
                empleado.setEdad(rs.getInt(10));
                empleado.setFechaIngreso(rs.getString(11));
              
                lista.add(empleado);
            }
        } catch (SQLException ex) {
            Logger.getLogger(dbEmpleado.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return lista;
    }
    
    public void update(Empleado empleado){
        
        Conexion conexion = new Conexion();
        conexion.conectar();
        String sql;
        if(empleado.getFoto().equals("")){
            sql = "UPDATE EMPLEADO SET nombre=?, apellidoM=?, apellidoP=?, puesto=?, rfc=?, edad=? WHERE codigo=?";
            
            try {
                PreparedStatement ps = conexion.getConexion().prepareStatement(sql);
                ps.setString(1, empleado.getNombre());
                ps.setString(2, empleado.getApellidoM());
                ps.setString(3, empleado.getApellidoP());
                ps.setString(4, empleado.getPuesto());
                ps.setString(5, empleado.getRFC());
                ps.setInt(6, empleado.getEdad());

                ps.setInt(7, empleado.getCodigo());
                ps.executeUpdate();

            } catch (SQLException ex) {
                Logger.getLogger(dbEmpleado.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else{
          sql = "UPDATE EMPLEADO SET nombre=?, apellidoM=?, apellidoP=?, puesto=?, rfc=?, foto=?, edad=? WHERE codigo=?";
          try {
                PreparedStatement ps = conexion.getConexion().prepareStatement(sql);
                ps.setString(1, empleado.getNombre());
                ps.setString(2, empleado.getApellidoM());
                ps.setString(3, empleado.getApellidoP());
                ps.setString(4, empleado.getPuesto());
                ps.setString(5, empleado.getRFC());
                ps.setString(6, empleado.getFoto());
                ps.setInt(7, empleado.getEdad());

                ps.setInt(8, empleado.getCodigo());
                ps.executeUpdate();

            } catch (SQLException ex) {
                Logger.getLogger(dbEmpleado.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }
    
    public int obtenerID(){
        Conexion conexion = new Conexion();
        conexion.conectar();
        
        int valor = 0;
        String sql = "SELECT IDENT_CURRENT('Empleado') as Incremento";
        
        try {
            PreparedStatement ps = conexion.getConexion().prepareStatement(sql);
            
            ResultSet rs = ps.executeQuery();
            
            while(rs.next()){
                valor = rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(dbEmpleado.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return valor;
    }
}

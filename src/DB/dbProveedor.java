/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DB;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import restaurant.Clases.Proveedor;

/**
 *
 * @author gnr_a
 */
public class dbProveedor {
    
    public void insert(Proveedor proveedor){
        Conexion conexion = new Conexion();
        conexion.conectar();        
       // byte[] foto = Files.readAllBytes(null);

        String sql= "INSERT INTO PROVEEDOR(nombre,direccion,telefono) VALUES (?,?,?)";
        try {
            
            PreparedStatement ps = conexion.getConexion().prepareStatement(sql);
            
            ps.setString(1, proveedor.getNombre());
            ps.setString(2, proveedor.getDomicilio());
            ps.setString(3, proveedor.getTelefono());
            
            ps.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(dbEmpleado.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void delete(int codigo){
        Conexion conexion = new Conexion();
        conexion.conectar();        
       // byte[] foto = Files.readAllBytes(null);
        String sql= "DELETE FROM Proveedor WHERE id=?";
        try {
            
            PreparedStatement ps = conexion.getConexion().prepareStatement(sql);   
            ps.setInt(1, codigo);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(dbEmpleado.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public List<Proveedor> read(){
        
        List<Proveedor> lista = new ArrayList<>();
        
        Conexion conexion = new Conexion();
        conexion.conectar();
        
       // byte[] foto = Files.readAllBytes(null);
        String sql= "SELECT * FROM PROVEEDOR";
        try {
            
            PreparedStatement ps = conexion.getConexion().prepareStatement(sql);   
            ResultSet rs = ps.executeQuery();
            
            while (rs.next()){
                Proveedor proveedor = new Proveedor();
                
                proveedor.setId(rs.getInt(1));
                proveedor.setNombre(rs.getString(2));
                proveedor.setDomicilio(rs.getString(3));
                proveedor.setTelefono(rs.getString(4));
                
                lista.add(proveedor);
            }
        } catch (SQLException ex) {
            Logger.getLogger(dbEmpleado.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return lista;
    }
    
    public Proveedor readOnly(int codigo){
        Proveedor proveedor = new Proveedor();
        
        Conexion conexion = new Conexion();
        conexion.conectar();        
       // byte[] foto = Files.readAllBytes(null);
        String sql= "SELECT * FROM Proveedor WHERE id=?";
        try {
            PreparedStatement ps = conexion.getConexion().prepareStatement(sql);
            ps.setInt(1, codigo);
            ResultSet rs = ps.executeQuery();

            while (rs.next()){
                proveedor.setId(rs.getInt(1));
                proveedor.setNombre(rs.getString(2));
                proveedor.setDomicilio(rs.getString(3));
                proveedor.setTelefono(rs.getString(4));
            }
        } catch (SQLException ex) {
            Logger.getLogger(dbEmpleado.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return proveedor;
        
    }
    
    public void update(Proveedor proveedor){
        Conexion conexion = new Conexion();
        conexion.conectar();
        
        String sql="UPDATE PROVEEDOR SET nombre=?, direccion=?, telefono=? WHERE id=?";
        
        try {
            PreparedStatement ps = conexion.getConexion().prepareStatement(sql);
            ps.setString(1, proveedor.getNombre());
            ps.setString(2, proveedor.getDomicilio());
            ps.setString(3, proveedor.getTelefono());
            ps.setInt(4, proveedor.getId());
            
            ps.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(dbProveedor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

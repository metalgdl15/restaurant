/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DB;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import restaurant.Clases.Comanda;
import restaurant.Clases.Empleado;
import restaurant.Clases.Platillo;

/**
 *
 * @author gnr_a
 */
public class dbComanda {
    
    //Para abrir una nueva comanda
    public void instert(Comanda comanda){
        String sql = "INSERT INTO Comanda (codigoMesero,NombreCliente,numeroMesa,Status,fechaInicio) VALUES (?,?,?,?,?)";
        
        Conexion conexion = new Conexion();
        conexion.conectar();
        
        try {
            PreparedStatement ps = conexion.getConexion().prepareStatement(sql);
            ps.setInt(1,comanda.getMesero().getCodigo());
            ps.setString(2, comanda.getCliente());
            ps.setString(3, comanda.getMesa());
            ps.setString(4, comanda.getStatus());
            ps.setString(5, comanda.getFechaIngreso());
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(dbComanda.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //asignar un platillo dentro de la comanda
    public void insertPlatillo(int comanda, int platillo){
        String sql = "INSERT INTO comanda_platillo (idComanda,idPlatillo, cantidad) VALUES (?,?,?)";
        
        Conexion conexion = new Conexion();
        conexion.conectar();
        
        try {
            PreparedStatement ps = conexion.getConexion().prepareStatement(sql);
            ps.setInt(1,comanda);
            ps.setInt(2, platillo);
            ps.setInt(3, 1);
            
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(dbComanda.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //Aumenar la cantidad de platillos dentro de una comanda
    public void addPlatillo(int comanda, int platillo){
        Conexion conexion = new Conexion();
        conexion.conectar();
        
        String sql = "UPDATE comanda_platillo set cantidad=cantidad+1 Where idComanda=? AND idPlatillo=?";
        
        try {
            PreparedStatement ps = conexion.getConexion().prepareStatement(sql);
            ps.setInt(1, comanda);
            ps.setInt(2, platillo);
            
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(dbComanda.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //Disminuir la cantidad de platillos en caso de cancelar
    public void decrementPlatillo(int comanda, int platillo){
        Conexion conexion = new Conexion();
        conexion.conectar();
        
        String sql = "UPDATE comanda_platillo set cantidad=cantidad-1 Where idComanda=? AND idPlatillo=?";
        
        try {
            PreparedStatement ps = conexion.getConexion().prepareStatement(sql);
            ps.setInt(1, comanda);
            ps.setInt(2, platillo);
            
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(dbComanda.class.getName()).log(Level.SEVERE, null, ex);
        }
    }   
    
    //Eliminar el platillo si llega a cero
    public void deletePlatillo(int comanda, int platillo){
        Conexion conexion = new Conexion();
        conexion.conectar();
        
        String sql = "DELETE Comanda_Platillo WHERE idComanda=? AND idPlatillo=?";
        
        try {
            PreparedStatement ps = conexion.getConexion().prepareStatement(sql);
            ps.setInt(1, comanda);
            ps.setInt(2, platillo);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(dbComanda.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //Para asignar un cocinero dentro de una comanda
    public void updateCocinero(int codigo, int folio){
        Conexion conexion = new Conexion();
        conexion.conectar();
        String sql="UPDATE comanda SET codigoCocinero=?, status='Proceso' WHERE folio=?";
        
        try {
            PreparedStatement ps = conexion.getConexion().prepareStatement(sql);
            ps.setInt(1, codigo);
            ps.setInt(2, folio);
            
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(dbComanda.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //Modificar el estatus de la comanda si este ya se preparo por el cocinero
    public void updateComanda(int folio){
        Conexion conexion = new Conexion();
        conexion.conectar();
        String sql="UPDATE comanda SET status='Elaborada' WHERE folio=?";
        
        try {
            PreparedStatement ps = conexion.getConexion().prepareStatement(sql);
            ps.setInt(1, folio);
            
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(dbComanda.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //Asignar a un cocinero
    public void updateCajero(String caja, int cajero){
        Conexion conexion = new Conexion();
        conexion.conectar();
        String sql="UPDATE comanda SET caja=?, cajero=? WHERE folio=" ;
        
        try {
            PreparedStatement ps = conexion.getConexion().prepareStatement(sql);
            ps.setString(1, caja);
            ps.setInt(2, cajero);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(dbComanda.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //Termminar la comanda
    public void terminarComanda(int folio, String fecha ,String pago){
        Conexion conexion = new Conexion();
        conexion.conectar();
        String sql="UPDATE comanda SET status='Cerrada', pago=?,fechaFin=? WHERE folio=?";
        
        try {
            PreparedStatement ps = conexion.getConexion().prepareStatement(sql);
            ps.setString(1, pago);
            ps.setString(2, fecha);
            ps.setInt(3, folio);
            
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(dbComanda.class.getName()).log(Level.SEVERE, null, ex);
        }   
    }
    
   //Le las comandas que tiene asiganado el mesero 
   public List<Comanda> read(int mesero){
       Conexion conexion = new Conexion();
       conexion.conectar();
       //Para obtener los datos del empleado y pasarlos a comanda si hacer los join
       dbEmpleado db = new dbEmpleado();
       
       List<Comanda> lista = new ArrayList<>();
       
       String sql="SELECT * FROM Comanda WHERE codigoMesero = ?";
        try {
            PreparedStatement ps = conexion.getConexion().prepareStatement(sql);
            ps.setInt(1, mesero);
            
            ResultSet rs = ps.executeQuery();
            
            while(rs.next()){
                Comanda comanda = new Comanda();                
                comanda.setFolio(rs.getInt(1));
                comanda.setMesero(db.readOnly(rs.getInt(2)));
                comanda.setCliente(rs.getString(3));
                comanda.setMesa(rs.getString(4));
                comanda.setCocinero(db.readOnly(rs.getInt(5)));
                comanda.setStatus(rs.getString(6));
                comanda.setCaja(rs.getString(7));
                comanda.setCajero(db.readOnly(rs.getInt(8)));
                comanda.setFechaIngreso(rs.getString(9));
                comanda.setFechaSalida(rs.getString(10));
                lista.add(comanda);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(dbComanda.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return lista;
   }
   
   //Leer todas las comandas
   public List<Comanda> readComanda(){
       Conexion conexion = new Conexion();
       conexion.conectar();
       
       //Para obtener los datos del empleado y pasarlos a comanda si hacer los join
       dbEmpleado db = new dbEmpleado();
       
       List<Comanda> lista = new ArrayList<>();
       
       String sql="SELECT * FROM Comanda";
        try {
            PreparedStatement ps = conexion.getConexion().prepareStatement(sql);
            
            ResultSet rs = ps.executeQuery();
            
            while(rs.next()){
                Comanda comanda = new Comanda();                
                comanda.setFolio(rs.getInt(1));
                comanda.setMesero(db.readOnly(rs.getInt(2)));
                comanda.setCliente(rs.getString(3));
                comanda.setMesa(rs.getString(4));
                comanda.setCocinero(db.readOnly(rs.getInt(5)));
                comanda.setStatus(rs.getString(6));
                comanda.setCaja(rs.getString(7));
                comanda.setCajero(db.readOnly(rs.getInt(8)));
                comanda.setFechaIngreso(rs.getString(9));
                comanda.setFechaSalida(rs.getString(10));
                lista.add(comanda);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(dbComanda.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return lista;
   }
   
   //Para verificar si encotro un un platillo dentro de una comanda
   public boolean verficarPlatilloComanda(int comanda, int platillo){
       Conexion conexion = new Conexion();
       conexion.conectar();
       
       String sql = "Select * From comanda_platillo where idComanda=? AND idPlatillo=?";
       
        try {
            PreparedStatement ps = conexion.getConexion().prepareStatement(sql);
            ps.setInt(1, comanda);
            ps.setInt(2, platillo);
            
            ResultSet rs = ps.executeQuery();
            
            int i=0;
            while (rs.next()){
                i++;
            }
            
           return i>0;
                
            
        } catch (SQLException ex) {
            Logger.getLogger(dbComanda.class.getName()).log(Level.SEVERE, null, ex);
        }
       return false;
   }
   
   //Obtener la lista de platillo que se guardo en una comanda
   public List<Platillo> readPlatillos(int idComanda){
       Conexion conexion = new Conexion();
       conexion.conectar();
       
       String sql = "SELECT p.*, cp.cantidad FROM platillo p INNER JOIN comanda_platillo cp ON cp.idPlatillo = p.id WHERE cp.idComanda=?";
       
       List<Platillo> lista = new ArrayList<>();
       
        try {
            PreparedStatement ps = conexion.getConexion().prepareStatement(sql);
            ps.setInt(1, idComanda);
          
            ResultSet rs= ps.executeQuery();
            
            while(rs.next()){
                Platillo platillo = new Platillo();
                             
                platillo.setId(rs.getInt(1));
                platillo.setNombre(rs.getString(2));
                platillo.setPrecio(rs.getDouble(3));
                platillo.setDescripcion(rs.getString(4));
                platillo.setImagen(rs.getString(5));
                platillo.setTipo(rs.getString(6));
                platillo.setDisponibilidad(rs.getString(7));
                platillo.setCantidad(rs.getInt(8));
                
                lista.add(platillo);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(dbComanda.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return lista;
   }
   
   //Buscar comanda a traves de un like
   public List<Comanda> searchComanda(String folio){
       Conexion conexion = new Conexion();
       conexion.conectar();
       
       //Para obtener los datos del empleado y pasarlos a comanda si hacer los join
       dbEmpleado db = new dbEmpleado();
       
       List<Comanda> lista = new ArrayList<>();
       
       String sql="SELECT * FROM Comanda WHERE folio LIKE ?";
       
        try {
            PreparedStatement ps = conexion.getConexion().prepareStatement(sql);
            ps.setString(1, "%"+folio);
            ResultSet rs = ps.executeQuery();
            
            while(rs.next()){
                Comanda comanda = new Comanda();                
                comanda.setFolio(rs.getInt(1));
                comanda.setMesero(db.readOnly(rs.getInt(2)));
                comanda.setCliente(rs.getString(3));
                comanda.setMesa(rs.getString(4));
                comanda.setCocinero(db.readOnly(rs.getInt(5)));
                comanda.setStatus(rs.getString(6));
                comanda.setCaja(rs.getString(7));
                comanda.setCajero(db.readOnly(rs.getInt(8)));
                comanda.setFechaIngreso(rs.getString(9));
                comanda.setFechaSalida(rs.getString(10));
                lista.add(comanda);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(dbComanda.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return lista;
   }

   //Buscar comanda deacuerdo al cocinero
   public List<Comanda> comandaCocinero(int cocinero){
       Conexion conexion = new Conexion();
       conexion.conectar();
       
       //Para obtener los datos del empleado y pasarlos a comanda si hacer los join
       dbEmpleado db = new dbEmpleado();
       
       List<Comanda> lista = new ArrayList<>();
       
       String sql="SELECT * FROM Comanda WHERE codigoCocinero=? AND status='Proceso'";
       
        try {
            PreparedStatement ps = conexion.getConexion().prepareStatement(sql);
            ps.setInt(1, cocinero);
            ResultSet rs = ps.executeQuery();
            
            while(rs.next()){
                Comanda comanda = new Comanda();                
                comanda.setFolio(rs.getInt(1));
                comanda.setMesero(db.readOnly(rs.getInt(2)));
                comanda.setCliente(rs.getString(3));
                comanda.setMesa(rs.getString(4));
                comanda.setCocinero(db.readOnly(rs.getInt(5)));
                comanda.setStatus(rs.getString(6));
                comanda.setCaja(rs.getString(7));
                comanda.setCajero(db.readOnly(rs.getInt(8)));
                comanda.setFechaIngreso(rs.getString(9));
                comanda.setFechaSalida(rs.getString(10));
                lista.add(comanda);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(dbComanda.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return lista;
   }
   
   //Buscar por el estatus
   public List<Comanda> readComandaStatus(){
       Conexion conexion = new Conexion();
       conexion.conectar();
       
       //Para obtener los datos del empleado y pasarlos a comanda si hacer los join
       dbEmpleado db = new dbEmpleado();
       
       List<Comanda> lista = new ArrayList<>();
       
       String sql="SELECT * FROM Comanda WHERE status ='Activa'";
       
        try {
            PreparedStatement ps = conexion.getConexion().prepareStatement(sql);
           
            ResultSet rs = ps.executeQuery();
            
            while(rs.next()){
                Comanda comanda = new Comanda();                
                comanda.setFolio(rs.getInt(1));
                comanda.setMesero(db.readOnly(rs.getInt(2)));
                comanda.setCliente(rs.getString(3));
                comanda.setMesa(rs.getString(4));
                comanda.setCocinero(db.readOnly(rs.getInt(5)));
                comanda.setStatus(rs.getString(6));
                comanda.setCaja(rs.getString(7));
                comanda.setCajero(db.readOnly(rs.getInt(8)));
                comanda.setFechaIngreso(rs.getString(9));
                comanda.setFechaSalida(rs.getString(10));
                
                lista.add(comanda);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(dbComanda.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
   }
   
   public int obtenerID(){
        Conexion conexion = new Conexion();
        conexion.conectar();
        
        int valor = 0;
        String sql = "SELECT IDENT_CURRENT('Comanda') as Incremento";
        
        try {
            PreparedStatement ps = conexion.getConexion().prepareStatement(sql);
            
            ResultSet rs = ps.executeQuery();
            
            while(rs.next()){
                valor = rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(dbEmpleado.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return valor;
   }
}

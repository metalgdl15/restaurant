/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DB;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import restaurant.Clases.Platillo;

/**
 *
 * @author gnr_a
 */
public class dbPlatillo {
    public void insert(Platillo platillo){
        Conexion conexion = new Conexion();
        conexion.conectar();        
       // byte[] foto = Files.readAllBytes(null);

        String sql= "INSERT INTO PLATILLO (nombre,precio,descripcion, imagen, tipo,disponibilidad) VALUES (?,?,?,?,?,?)";
        try {
            
            PreparedStatement ps = conexion.getConexion().prepareStatement(sql);
            
            ps.setString(1, platillo.getNombre());
            ps.setDouble(2, platillo.getPrecio());
            ps.setString(3, platillo.getDescripcion());
            ps.setString(4, platillo.getImagen());
            ps.setString(5, platillo.getTipo());
            ps.setString(6, platillo.getDisponibilidad());
                        
            ps.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(dbEmpleado.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void delete(int codigo){
        Conexion conexion = new Conexion();
        conexion.conectar();        
    
        String sql= "DELETE FROM Platillo WHERE id=?";
        try {
            
            PreparedStatement ps = conexion.getConexion().prepareStatement(sql);   
            ps.setInt(1, codigo);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(dbEmpleado.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public List<Platillo> read(){
        
        List<Platillo> lista = new ArrayList<>();
        
        Conexion conexion = new Conexion();
        conexion.conectar();
        
       // byte[] foto = Files.readAllBytes(null);
        String sql= "SELECT * FROM PLATILLO";
        try {
            
            PreparedStatement ps = conexion.getConexion().prepareStatement(sql);   
            ResultSet rs = ps.executeQuery();
            
            while (rs.next()){
                Platillo platillo = new Platillo();
                
                platillo.setId(rs.getInt(1));
                platillo.setNombre(rs.getString(2));
                platillo.setPrecio(rs.getDouble(3));
                platillo.setDescripcion(rs.getString(4));
                platillo.setImagen(rs.getString(5));
                platillo.setDisponibilidad(rs.getString(7));
                
                lista.add(platillo);
            }
        } catch (SQLException ex) {
            Logger.getLogger(dbEmpleado.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return lista;
    }
    
    public List<Platillo> readSearch(String tipo){
        List<Platillo> lista = new ArrayList<>();
        
        Conexion conexion = new Conexion();
        conexion.conectar();        
       // byte[] foto = Files.readAllBytes(null);
        String sql= "SELECT * FROM Platillo WHERE tipo=?";
        try {
            PreparedStatement ps = conexion.getConexion().prepareStatement(sql);
            ps.setString(1, tipo);
            ResultSet rs = ps.executeQuery();

            while (rs.next()){
                Platillo platillo = new Platillo();
                platillo.setId(rs.getInt(1));
                platillo.setNombre(rs.getString(2));
                platillo.setPrecio(rs.getDouble(3));
                platillo.setDescripcion(rs.getString(4));
                platillo.setImagen(rs.getString(5));
                platillo.setTipo(rs.getString(6));
                platillo.setDisponibilidad(rs.getString(7));
                lista.add(platillo);
            }
        } catch (SQLException ex) {
            Logger.getLogger(dbEmpleado.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return lista;
    }
    
    public List<Platillo> readDisponible(String tipo){
        List<Platillo> lista = new ArrayList<>();
        
        Conexion conexion = new Conexion();
        conexion.conectar();        
       // byte[] foto = Files.readAllBytes(null);
        String sql= "SELECT * FROM Platillo WHERE tipo=? AND disponibilidad='Si'";
        try {
            PreparedStatement ps = conexion.getConexion().prepareStatement(sql);
            ps.setString(1, tipo);
            ResultSet rs = ps.executeQuery();

            while (rs.next()){
                Platillo platillo = new Platillo();
                platillo.setId(rs.getInt(1));
                platillo.setNombre(rs.getString(2));
                platillo.setPrecio(rs.getDouble(3));
                platillo.setDescripcion(rs.getString(4));
                platillo.setImagen(rs.getString(5));
                platillo.setTipo(rs.getString(6));
                platillo.setDisponibilidad(rs.getString(7));
                lista.add(platillo);
            }
        } catch (SQLException ex) {
            Logger.getLogger(dbEmpleado.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return lista;
    }
    
    public void update(Platillo platillo){
        Conexion conexion = new Conexion();
        conexion.conectar();
        
        String sql;
        
        if(platillo.getImagen().equals("")){
            sql="UPDATE PLATILLO SET nombre=?, precio=?, Descripcion=?, tipo=?, disponibilidad=? WHERE id=?";
            try {
                PreparedStatement ps = conexion.getConexion().prepareStatement(sql);

                ps.setString(1, platillo.getNombre());
                ps.setDouble(2, platillo.getPrecio());
                ps.setString(3, platillo.getDescripcion());
                ps.setString(4, platillo.getTipo());
                ps.setString(5, platillo.getDisponibilidad());
                ps.setInt(6, platillo.getId());
            
                ps.executeUpdate();
            } catch (SQLException ex) {
                Logger.getLogger(dbPlatillo.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }else{
            sql="UPDATE PLATILLO SET nombre=?, precio=?, Descripcion=?, tipo=?, imagen=?, disponibilidad=? WHERE id=?";
            try {
                PreparedStatement ps = conexion.getConexion().prepareStatement(sql);

                ps.setString(1, platillo.getNombre());
                ps.setDouble(2, platillo.getPrecio());
                ps.setString(3, platillo.getDescripcion());
                ps.setString(4, platillo.getTipo());
                ps.setString(5, platillo.getImagen());
                ps.setString(6, platillo.getDisponibilidad());
                ps.setInt(7, platillo.getId());
               
                ps.executeUpdate();
            } catch (SQLException ex) {
                Logger.getLogger(dbPlatillo.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }
    
    public int obtenerID(){
         Conexion conexion = new Conexion();
        conexion.conectar();
        
        int valor = 0;
        String sql = "SELECT IDENT_CURRENT('Platillo') as Incremento";
        
        try {
            PreparedStatement ps = conexion.getConexion().prepareStatement(sql);
            
            ResultSet rs = ps.executeQuery();
            
            while(rs.next()){
                valor = rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(dbEmpleado.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return valor;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DB;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import restaurant.Clases.Empleado;

/**
 *
 * @author gnr_a
 */
public class dbUsuario {
    
    public Empleado Verificar(int usuario, String clave){
        Conexion conexion = new Conexion();
        conexion.conectar();
        
        String sql="SELECT e.* FROM usuario u INNER JOIN Empleado e ON u.codigo=e.codigo WHERE u.codigo=? AND u.password=?";
        Empleado empleado = new Empleado();
        
        try {
            PreparedStatement ps = conexion.getConexion().prepareStatement(sql);
            ps.setInt(1, usuario);
            ps.setString(2, clave);
            
            ResultSet rs = ps.executeQuery();
            String tipo;
            while (rs.next()){
                
                
                empleado.setCodigo(rs.getInt(1));
                empleado.setNombre(rs.getString(2));
                empleado.setApellidoP(rs.getString(3));
                empleado.setApellidoM(rs.getString(4));
                empleado.setPuesto(rs.getString(5));              
                empleado.setRFC(rs.getString(8));
                empleado.setFoto(rs.getString(9));
                empleado.setEdad(rs.getInt(10));
                empleado.setFechaIngreso(rs.getString(11)); 
                
              
            }
        } catch (SQLException ex) {
            Logger.getLogger(dbUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return empleado;
    }
    
}

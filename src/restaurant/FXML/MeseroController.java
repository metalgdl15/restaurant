/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restaurant.FXML;

import DB.dbComanda;
import DB.dbEmpleado;
import DB.dbPlatillo;
import herramientas.isNumeric;
import herramientas.statusComanda;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Duration;
import restaurant.Clases.Comanda;
import restaurant.Clases.Empleado;
import restaurant.Clases.Platillo;

/**
 * FXML Controller class
 *
 * @author gnr_a
 */
public class MeseroController implements Initializable {

    
    private String tipo;
    private Empleado mesero;
    
    @FXML private RadioButton btnMariscos, btnJaponesa, btnBebidas;
    @FXML private Label fecha, lblDatos, lblFolio;
    @FXML private TextField txtMesa, txtCliente;
    @FXML private RadioButton btnEfectivo, btnCredito;
    /**
     * Initializes the controller class.
     */
    
    //tabla de comandas
    @FXML TableView <Comanda> tablaComanda;
    @FXML TableColumn <Comanda, Integer> columnFolio;
    @FXML TableColumn <Comanda, String> columnMesa;
    @FXML TableColumn <Comanda, String> columnStatus;
    ObservableList <Comanda> oblistCom= FXCollections.observableArrayList();
    
    //Tabla de los platillos
    @FXML TableView <Platillo> tablaPlatillos;
    @FXML TableColumn <Platillo, Integer> columnCodigoPla;
    @FXML TableColumn <Platillo, String> columnPlatilloPla;
    ObservableList <Platillo> oblistPla= FXCollections.observableArrayList();
    
    //Tabla de la comanda con los platillos
    @FXML TableView <Platillo> tablaComandaPlatillo;
    @FXML TableColumn <Platillo, Integer> columnCantidadCom;
    @FXML TableColumn <Platillo, String> columnPlatilloCom;
    ObservableList <Platillo> oblistComPla= FXCollections.observableArrayList();
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        dbEmpleado db = new dbEmpleado();
        //this.mesero = db.readOnly(6);
        
        iniciaReloj();
        this.tipo = "Japonesa";
        obtenerPlatillos(this.tipo);
        
        
        ToggleGroup group = new ToggleGroup();
        btnBebidas.setToggleGroup(group);
        btnJaponesa.setToggleGroup(group);
        btnMariscos.setToggleGroup(group);      
        
        ToggleGroup group2 = new ToggleGroup();
        btnEfectivo.setToggleGroup(group2);
        btnCredito.setToggleGroup(group2);
    }
    
    private void iniciaReloj(){
        Timeline time = new Timeline(new KeyFrame(Duration.ZERO, ev ->{
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/YYYY HH:mm:ss");
            fecha.setText(LocalDateTime.now().format(formatter));
                
        }),new KeyFrame(Duration.seconds(1)));
        time.setCycleCount(Animation.INDEFINITE);
        time.play();
    }
    
    public void usuario(Empleado empleado){
        this.mesero = empleado;
        obtenerComandas();
    }
        
     
    
    private void obtenerPlatillos(String type){
        tablaPlatillos.getItems().clear();
        dbPlatillo db = new dbPlatillo();
        
        List<Platillo> lista = db.readDisponible(type);
        Iterator <Platillo> i= lista.iterator();
        
        while(i.hasNext()){
            oblistPla.add(i.next());
        }
        
        columnCodigoPla.setCellValueFactory(new PropertyValueFactory("id"));
        columnPlatilloPla.setCellValueFactory(new PropertyValueFactory("nombre"));
        
        tablaPlatillos.setItems(oblistPla);
    } 
   
    private void obtenerComandas(){
        tablaComanda.getItems().clear();
        dbComanda db = new dbComanda();
        List<Comanda> lista = db.read(this.mesero.getCodigo());
        Iterator<Comanda> i = lista.iterator();
        
        while(i.hasNext()){
            oblistCom.add(i.next());
        }
        
        columnFolio.setCellValueFactory(new PropertyValueFactory("folio"));
        columnMesa.setCellValueFactory(new PropertyValueFactory("mesa"));
        columnStatus.setCellValueFactory(new PropertyValueFactory("status"));
        
        tablaComanda.setItems(oblistCom);
    }
    
    private void obtenerComandaPlatillo(){
        
        tablaComandaPlatillo.getItems().clear();
        dbComanda db = new dbComanda();
        
        int folio = Integer.parseInt(lblFolio.getText());
        System.out.println(folio);
        List<Platillo> lista = db.readPlatillos(folio);
        Iterator <Platillo> i= lista.iterator();
        
        while(i.hasNext()){
            oblistComPla.add(i.next());
        }
        
        columnCantidadCom.setCellValueFactory(new PropertyValueFactory("cantidad"));
        columnPlatilloCom.setCellValueFactory(new PropertyValueFactory("nombre"));
        
        tablaComandaPlatillo.setItems(oblistComPla);
    }
   
    @FXML
    private void tipoPlatillo(ActionEvent event){
        if(btnBebidas.isSelected()){
            this.tipo = "Bebidas";
        }else if(btnJaponesa.isSelected()){
            this.tipo = "Japonesa";
        }else{
            this.tipo = "Mariscos";
        }
        
        obtenerPlatillos(this.tipo);
    }
    
    @FXML
    private  void ingresarComanda(){
        dbComanda db = new dbComanda();
        Comanda comanda = new Comanda();
        if(new isNumeric(txtMesa.getText()).numerico){
            comanda.setMesa(txtMesa.getText());
            System.out.println(comanda.getMesa());
        }
        
        comanda.setCliente(txtCliente.getText());
        comanda.setFechaIngreso(fecha.getText());
        
        comanda.setMesero(this.mesero);
        comanda.setStatus(statusComanda.Activa.name());
        db.instert(comanda);
        obtenerComandas();
    }
    
    @FXML
    private void asignarPlatillos(){
        dbComanda db = new dbComanda();
        Platillo platillo = tablaPlatillos.getSelectionModel().getSelectedItem();
        
        int comanda = Integer.parseInt(lblFolio.getText());
        int plato = platillo.getId();
        
       
        if(db.verficarPlatilloComanda(comanda, plato)){
            
            db.addPlatillo(comanda, plato);
        }else{
            db.insertPlatillo(comanda, plato);
        }
      
        obtenerComandaPlatillo();
    }
    
    
    
    @FXML
    private void decrementarPlatillos(){
        dbComanda db = new dbComanda();
        Platillo platillo = tablaComandaPlatillo.getSelectionModel().getSelectedItem();
        
        int comanda = Integer.parseInt(lblFolio.getText());
        int plato = platillo.getId();
        
        if(platillo.getCantidad() == 1){
            db.deletePlatillo(comanda, plato);
        }else{
            db.decrementPlatillo(comanda, plato);
        }
        
        obtenerComandaPlatillo();
            
    }
    
    
    @FXML void seleccionarComanda(ActionEvent event){
        Comanda comanda = tablaComanda.getSelectionModel().getSelectedItem();
        lblDatos.setText("Mesa: "+comanda.getMesa()+
                "\nCliente: "+comanda.getCliente()+
                "\nCocinero: "+comanda.getCocinero());
        
        lblFolio.setText(String.valueOf(comanda.getFolio()));
        
        obtenerComandaPlatillo();
    }
   
    @FXML void terminarComanda(ActionEvent event){
        String fechaActual = fecha.getText();
        dbComanda db = new dbComanda();
        int folio = Integer.parseInt(lblFolio.getText());
        
        if (btnEfectivo.isSelected()){
            db.terminarComanda(folio, fechaActual, "Efectivo");
        }else if(btnCredito.isSelected()){
            db.terminarComanda(folio, fechaActual ,"Credito");
        }else{
            
        }
        
        obtenerComandas();
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restaurant.FXML;

import DB.dbComanda;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import restaurant.Clases.Comanda;
import restaurant.Clases.Empleado;
import restaurant.Clases.Platillo;

/**
 * FXML Controller class
 *
 * @author gnr_a
 */
public class CocinaController implements Initializable {
    
    private Empleado usuario;

    @FXML TableView <Comanda> tablaComanda;
    @FXML TableColumn <Comanda, Integer> columnFolio;
    @FXML TableColumn <Comanda, String> columnMesa;
    @FXML TableColumn <Comanda, String> columnStatus;
    @FXML TableColumn <Comanda, String> columnMesero;    
    ObservableList <Comanda> oblistCom= FXCollections.observableArrayList();
    
    @FXML TableView <Platillo> tablaPlatillo;
    @FXML TableColumn <Platillo, Integer> columnCantidad;
    @FXML TableColumn <Platillo, String> columnPlatillo;
    ObservableList <Platillo> oblistPla= FXCollections.observableArrayList();
    
    
    @FXML ComboBox boxComanda;
    @FXML Label lblStatus, lblPlatillos;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        obtenerComandas();
        actualizarBox();
    }
    
    public void usuario(Empleado empleado){
        this.usuario = empleado;
    }
    
    //Actualizar comboBox
    private void actualizarBox(){
        tablaPlatillo.getItems().clear();
        boxComanda.getItems().clear();
        
        ObservableList<String> items = FXCollections.observableArrayList();
        dbComanda db = new dbComanda();
        List<Comanda> lista = db.comandaCocinero(14);
        
        for(Comanda comanda : lista){
            items.add(String.valueOf(comanda.getFolio()));
        }
        
        boxComanda.setItems(items);
    }
    
    private void obtenerComandas(){
        tablaComanda.getItems().clear();
        dbComanda db = new dbComanda();
        
        List<Comanda> lista = db.readComandaStatus();
        
        for(Comanda comanda : lista){
            oblistCom.add(comanda);
        }
        
        columnFolio.setCellValueFactory(new PropertyValueFactory("folio"));
        columnMesero.setCellValueFactory(new PropertyValueFactory("mesero"));
        columnStatus.setCellValueFactory(new PropertyValueFactory("status"));
        columnMesa.setCellValueFactory(new PropertyValueFactory("mesa"));
        
        tablaComanda.setItems(oblistCom);
    }
    
    @FXML
    private void obtenerPlatillos(ActionEvent event){
        
        if (boxComanda.getValue() != null){
            tablaPlatillo.getItems().clear();
            int folio = Integer.parseInt(boxComanda.getValue().toString());

            dbComanda db = new dbComanda();

            List<Platillo> lista = db.readPlatillos(folio);

            for(Platillo platillo : lista){
                oblistPla.add(platillo);
            }

            columnCantidad.setCellValueFactory(new PropertyValueFactory("cantidad"));
            columnPlatillo.setCellValueFactory(new PropertyValueFactory("nombre"));

            tablaPlatillo.setItems(oblistPla);
        }
   
    }
    
    @FXML 
    private void asignarCocinero(){
        Comanda comanda = tablaComanda.getSelectionModel().getSelectedItem();
        dbComanda db = new dbComanda();
        
        db.updateCocinero(usuario.getCodigo(), comanda.getFolio());
        obtenerComandas();
        actualizarBox();
    }
    
    @FXML
    private void terminarComanda(ActionEvent event){
        int folio = Integer.parseInt(boxComanda.getValue().toString());
        dbComanda db = new dbComanda();
        db.updateComanda(folio);
        
        int index = boxComanda.getSelectionModel().getSelectedIndex();

        actualizarBox();
        
    }
    
    @FXML
    private void mostrarPlatillos(MouseEvent event){
        
        Comanda comanda = tablaComanda.getSelectionModel().getSelectedItem();
        System.out.println(comanda.getFolio());

            dbComanda db = new dbComanda();

            List<Platillo> lista = db.readPlatillos(comanda.getFolio());
            
            String cadenaPlatillos = "";
            
            for(Platillo platillo : lista){
                cadenaPlatillos += platillo.getCantidad() + "      -     " + platillo.getNombre() + "\n";
            }
            
            lblPlatillos.setText(cadenaPlatillos);

    }
    
}

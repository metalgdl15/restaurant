/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restaurant.FXML;

import herramientas.Puesto;
import herramientas.tipoPlatillo;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.*;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Duration;

import herramientas.baseLXIV;
import DB.dbEmpleado;
import DB.dbPlatillo;
import DB.dbProveedor;
import java.awt.image.BufferedImage;


import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javax.imageio.ImageIO;

import restaurant.Clases.*;



/**
 * FXML Controller class
 *
 * @author gnr_a
 */
public class AdministracionController implements Initializable {

        private Empleado usuario;
        
        private File file, filePla;
        private Image foto, imagenPla;
        private String tipoPla;
    //Datos de empleado


        @FXML private Button cargarFoto;
        @FXML private ImageView frame;
        @FXML private Label fecha1, lblCodigoEmp, nombreUser;
        @FXML private ComboBox boxPuestos;
        @FXML private TextField txtNombreEmp, txtApellidoM, txtApellidoP, txtEdad, txtRFC, txtBusquedaEmp;

       //Tabla de empleados
        @FXML private TableView<Empleado> tablaEmp;
        @FXML private TableColumn<Empleado, Integer> columnEdad, columnCodigo;
        @FXML private TableColumn<Empleado, String> columnNombre, columnCargo, columnRFC, columnFechaIngreso;
        ObservableList <Empleado> oblist= FXCollections.observableArrayList();
    ////////////////////////////////////////////////////////////////////////////
        
     //Datos proveedor
        @FXML private TextField txtNombreProv, txtDireccionProv, txtTelefonoProv;
        @FXML private Label lblCodigoPro;
        
        //Tabla de proveedor
        @FXML private TableView<Proveedor> tablaProveedor;
        @FXML private TableColumn<Proveedor, Integer> columnCodigoPro;
        @FXML private TableColumn<Proveedor, String> columnNombrePro, columnDireccionPro, columnTelefonoPro;
        ObservableList <Proveedor> oblistPro= FXCollections.observableArrayList();
    
    ///////////////////////////////////////////////////////////////////////////
     
    //Datos platillos
        @FXML private TextArea txtDescripcion;
        @FXML private TextField txtNombrePlatillo, txtPrecio, txtUrlPlatillo;
        @FXML private ImageView imagenPlatillo;
        @FXML private ComboBox boxTipoPlatillo, boxDisponibilidad;
        @FXML private RadioButton buttonJap, buttonMar, buttonBeb;
        @FXML private Label lblCodigoPla;
        
        //Tabla platillos
        @FXML private TableView<Platillo> tablaPlatillo;
        @FXML private TableColumn<Platillo, Double> columnPrecioP;
        @FXML private TableColumn<Platillo, Integer> columnIdP;
        @FXML private TableColumn<Platillo, String> columnPlatillo, columnDescripcionP, columnTipoP, columnDisponible;
        
        ObservableList <Platillo> oblistP= FXCollections.observableArrayList();
        
    private void iniciaReloj(){
        Timeline time = new Timeline(new KeyFrame(Duration.ZERO, ev ->{
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/YYYY HH:mm:ss");
            fecha1.setText(LocalDateTime.now().format(formatter));
                
        }),new KeyFrame(Duration.seconds(1)));
        time.setCycleCount(Animation.INDEFINITE);
        time.play();
    }
    
    public int CodigoActualEmpleado(){
        dbEmpleado db = new dbEmpleado();
        
        return db.obtenerID()+1;
    }
    
    public int CodigoActualPlatillo(){
        dbPlatillo db = new dbPlatillo();
        
        return db.obtenerID()+1;
    }
    
    public void usuario(Empleado empleado){
        this.usuario = empleado;
        
        nombreUser.setText(usuario.getNombre() +" "+usuario.getApellidoP());
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.tipoPla = "Japonesa";
        
        //Iniciamos el reloj
        iniciaReloj();
        
        //Muestra el id que continua en empleado y platillo
        lblCodigoEmp.setText(String.valueOf(CodigoActualEmpleado()));
        lblCodigoPla.setText(String.valueOf(CodigoActualPlatillo()));
        
        //Iniciamos las tablas
        obtenerEmpleados();
        obtenerPlatillos(this.tipoPla);
        obtenerProveedores();
        
        //Agregamos los datos a cada combobox
        
        boxPuestos.setItems( FXCollections.observableArrayList( Puesto.values()));
        boxTipoPlatillo.setItems( FXCollections.observableArrayList( tipoPlatillo.values()));
        ObservableList <String> oblistDis= FXCollections.observableArrayList();
        oblistDis.add("Si");    oblistDis.add("No");
        boxDisponibilidad.setItems(oblistDis);
        
        //Creamos un grupo para los radioButton
        ToggleGroup grupoButton = new ToggleGroup();
        buttonBeb.setToggleGroup(grupoButton);
        buttonMar.setToggleGroup(grupoButton);
        buttonJap.setToggleGroup(grupoButton);
        //buttonJap.selectedProperty();
    }    
    
    //Metodos para empleado
        //Cargar la foto del empleado
        @FXML
        private void cargarImg(ActionEvent event) {
                FileChooser chooser = new FileChooser();
                chooser.setTitle("Open File");

                chooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Image Files","*.bmp", "*.png", "*.jpg", "*.gif"));
                this.file = chooser.showOpenDialog(new Stage());

                String imagepath;
                try {
                    imagepath = this.file.toURI().toURL().toString();
                    this.foto = new Image(imagepath);

                    frame.setImage(this.foto);
                } catch (MalformedURLException ex) {
                    Logger.getLogger(AdministracionController.class.getName()).log(Level.SEVERE, null, ex);   
                }
        }

        //Metodo para obtener todos los empleados si agregar, eliminar o modificar y este sea en automatico
        public void obtenerEmpleados(){
            tablaEmp.getItems().clear();
            dbEmpleado db = new dbEmpleado();
//            baseLXIV base = new baseLXIV();

               List<Empleado> lista= db.read();           

            //   frame.setImage(base.decodeToImage(lista.get(2).getFoto()));
               Iterator <Empleado> i = lista.iterator();
               while(i.hasNext()){
                   oblist.add(i.next());
               }

               columnCodigo.setCellValueFactory(new PropertyValueFactory("Codigo"));
               columnNombre.setCellValueFactory(new PropertyValueFactory("nombre"));
               columnEdad.setCellValueFactory(new PropertyValueFactory("edad"));
               columnRFC.setCellValueFactory(new PropertyValueFactory("RFC"));
               columnCargo.setCellValueFactory(new PropertyValueFactory("puesto"));
               columnFechaIngreso.setCellValueFactory(new PropertyValueFactory("FechaIngreso"));

                tablaEmp.setItems(oblist);
                
        }
        
        //Buscar un tipo de empleado
        @FXML
        private void mostrarEmp(ActionEvent event){
           
            obtenerEmpleados();
        }
        
        //Limpiar los textos de empleado
        @FXML
        private void limpiarEmp(ActionEvent event){
            txtApellidoM.clear();
            txtApellidoP.clear();
            txtEdad.clear();
            txtNombreEmp.clear();
            txtRFC.clear();
            frame.setImage(null);
            lblCodigoEmp.setText("");
            lblCodigoEmp.setText(String.valueOf(CodigoActualEmpleado()));
            
        }
        
        //Eliminar un empleado seleccionado en la tabla
        @FXML
        private void eliminarEmp(ActionEvent event){
            dbEmpleado db = new dbEmpleado();
            Empleado empleado = tablaEmp.getSelectionModel().getSelectedItem();
            db.delete(empleado.getCodigo());
            obtenerEmpleados();
        }

        @FXML
        private void agregarEmp(ActionEvent event){
            //dbAdministrador base = new dbAdministrador();
            //base.instertar();

            Empleado empleado = new Empleado();
            dbEmpleado db = new dbEmpleado();

            if(boxPuestos.getValue() != null && txtEdad != null && txtNombreEmp !=null && txtApellidoM != null && txtApellidoP != null && txtRFC != null){
                Puesto puesto = (Puesto)boxPuestos.getValue();

                empleado.setApellidoM(txtApellidoM.getText());
                empleado.setApellidoP(txtApellidoP.getText());
                empleado.setFechaIngreso(fecha1.getText());

                empleado.setNombre(txtNombreEmp.getText());
                empleado.setEdad(Integer.parseInt(txtEdad.getText()));
                empleado.setRFC(txtRFC.getText());
                empleado.setFechaIngreso(fecha1.getText());

                empleado.setPuesto(puesto.name());

                //Agregar imagen a base 64
                try {
                    baseLXIV base = new baseLXIV();
                    BufferedImage imagen = ImageIO.read(this.file);
                    empleado.setFoto(base.encodeToString(imagen, "jpg"));
               
                    db.insert(empleado);
                    
                    this.file = null;

                    Alert alert = new Alert(AlertType.INFORMATION);
                alert.setHeaderText("SE REGISTRO CORRECTAMENTE");
                alert.showAndWait();
                obtenerEmpleados();
                
                lblCodigoEmp.setText(String.valueOf(CodigoActualEmpleado()));

                } catch (IOException ex) {
                    Logger.getLogger(AdministracionController.class.getName()).log(Level.SEVERE, null, ex);
                }

            }else{       
               Alert alert = new Alert(AlertType.ERROR);
               alert.setHeaderText("SE ENCONTRO UN ERROR");
               alert.setContentText("Revise que todos los campos esten llenos o que cumplan con el formato");
               alert.showAndWait();
            }

        }
        
        //Subir a barra para empleado
        @FXML
        private void upEmpleado(ActionEvent event){
            
            baseLXIV base = new baseLXIV();
            Empleado empleado = tablaEmp.getSelectionModel().getSelectedItem();
            
            txtNombreEmp.setText(empleado.getNombre());
            txtApellidoM.setText(empleado.getApellidoM());
            txtApellidoP.setText(empleado.getApellidoP());
            txtEdad.setText(String.valueOf(empleado.getEdad()));
            txtRFC.setText(empleado.getRFC());
            boxPuestos.setValue(Puesto.valueOf(empleado.getPuesto().replace(" ","")));
            
            frame.setImage(base.decodeToImage(empleado.getFoto()));
            
            
            lblCodigoEmp.setText(String.valueOf(empleado.getCodigo()));
        }
        
        //Actualizar datos
        @FXML
        private void actualizarEmp(ActionEvent event){
           Empleado empleado = new Empleado();
           dbEmpleado db = new dbEmpleado();
            
            if(boxPuestos.getValue() != null && txtEdad != null && txtNombreEmp !=null && txtApellidoM != null && txtApellidoP != null && txtRFC != null){
                System.out.println(boxPuestos.getValue());
                Puesto puesto =(Puesto) boxPuestos.getValue();
                
                empleado.setCodigo(Integer.parseInt(lblCodigoEmp.getText()));
                empleado.setApellidoM(txtApellidoM.getText());
                empleado.setApellidoP(txtApellidoP.getText());
                empleado.setFechaIngreso(fecha1.getText());

                empleado.setNombre(txtNombreEmp.getText());
                empleado.setEdad(Integer.parseInt(txtEdad.getText()));
                empleado.setRFC(txtRFC.getText());

                empleado.setPuesto(puesto.name());
                

                //Agregar imagen a base 64
                try {
                    if(this.file != null){
                        baseLXIV base = new baseLXIV();
                        BufferedImage imagen = ImageIO.read(this.file);
                        empleado.setFoto(base.encodeToString(imagen, "jpg"));
                    }else{
                        empleado.setFoto("");
                    }
                    db.update(empleado);
                    this.file = null;
//                    Alert alert = new Alert(AlertType.INFORMATION);
//                alert.setHeaderText("SE REGISTRO CORRECTAMENTE");
//                alert.showAndWait();
                    obtenerEmpleados();

                } catch (IOException ex) {
                    Logger.getLogger(AdministracionController.class.getName()).log(Level.SEVERE, null, ex);
                }

            }else{       
               Alert alert = new Alert(AlertType.ERROR);
               alert.setHeaderText("SE ENCONTRO UN ERROR");
               alert.setContentText("Revise que todos los campos esten llenos o que cumplan con el formato");
               alert.showAndWait();
            }

       }
        
        @FXML
        private void buscarEmp(KeyEvent event){
            String valor = txtBusquedaEmp.getText();
            tablaEmp.getItems().clear();
            
             if(valor.length()>0){
                dbEmpleado db= new dbEmpleado();
                
                List<Empleado> lista = db.readSearch(valor);
                
                Iterator <Empleado> i = lista.iterator();
               while(i.hasNext()){
                   oblist.add(i.next());
               }

               columnCodigo.setCellValueFactory(new PropertyValueFactory("Codigo"));
               columnNombre.setCellValueFactory(new PropertyValueFactory("nombre"));
               columnEdad.setCellValueFactory(new PropertyValueFactory("edad"));
               columnRFC.setCellValueFactory(new PropertyValueFactory("RFC"));
               columnCargo.setCellValueFactory(new PropertyValueFactory("puesto"));

                tablaEmp.setItems(oblist);
            }else{
                 obtenerEmpleados();
            }
            
        }
    ///////////////////////////////////////////////////////////////////////////////////////////  
        
    //Metodos para proveedor
        @FXML
        private void agregarProv(ActionEvent event){
            Proveedor proveedor = new Proveedor();
            dbProveedor db = new dbProveedor();
            
            proveedor.setNombre(txtNombreProv.getText());
            proveedor.setDomicilio(txtDireccionProv.getText());
            proveedor.setTelefono(txtTelefonoProv.getText());
            
            db.insert(proveedor);
            obtenerProveedores();
        }
        
        @FXML
        private void downProv(ActionEvent event){
            Proveedor proveedor = tablaProveedor.getSelectionModel().getSelectedItem();
            
            txtNombreProv.setText(proveedor.getNombre());
            txtDireccionProv.setText(proveedor.getDomicilio());
            txtTelefonoProv.setText(proveedor.getTelefono());
            lblCodigoPro.setText(String.valueOf(proveedor.getId()));
            
        }
        
        //Actualizar proveedor
        @FXML
        private void actualizarPro(ActionEvent event){
            dbProveedor db = new dbProveedor();
            Proveedor proveedor = new Proveedor();
            
            proveedor.setTelefono(txtTelefonoProv.getText());
            proveedor.setNombre(txtNombreProv.getText());
            proveedor.setDomicilio(txtDireccionProv.getText());
            proveedor.setId(Integer.parseInt(lblCodigoPro.getText()));
            
            db.update(proveedor);
            obtenerProveedores();
        }
        
        //Obtener lista de proveedores
        private void obtenerProveedores(){
            tablaProveedor.getItems().clear();
            dbProveedor db = new dbProveedor();
            List<Proveedor> lista = db.read();
            
            Iterator<Proveedor> i = lista.iterator();
            while(i.hasNext()){
                oblistPro.add(i.next());
            }
            
            columnCodigoPro.setCellValueFactory(new PropertyValueFactory("id"));
            columnNombrePro.setCellValueFactory(new PropertyValueFactory("nombre"));
            columnDireccionPro.setCellValueFactory(new PropertyValueFactory("domicilio"));
            columnTelefonoPro.setCellValueFactory(new PropertyValueFactory("telefono"));
            
            tablaProveedor.setItems(oblistPro);
        }
        
        //Eliminar proveedor
        @FXML
        private void eliminarPro(ActionEvent event){
            dbProveedor db = new dbProveedor();
            Proveedor proveedor = tablaProveedor.getSelectionModel().getSelectedItem();
            db.delete(proveedor.getId());
            obtenerProveedores();
        }

        @FXML
        private void limpiarProv(ActionEvent event){
            txtNombreProv.clear();
            txtTelefonoProv.clear();
            txtDireccionProv.clear();
            
        }
    ///////////////////////////////////////////////////////////////////////////////////////////
        
    //Metodos para platillos
        @FXML
        private void cargarImgPla(ActionEvent event){
            FileChooser chooser = new FileChooser();
                chooser.setTitle("Open File");

                chooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Image Files","*.bmp", "*.png", "*.jpg", "*.gif"));
                this.filePla = chooser.showOpenDialog(new Stage());

                String imagepath;
                try {
                    imagepath = this.filePla.toURI().toURL().toString();
                    this.imagenPla = new Image(imagepath);

                    txtUrlPlatillo.setText(imagepath);
                } catch (MalformedURLException ex) {
                    Logger.getLogger(AdministracionController.class.getName()).log(Level.SEVERE, null, ex);   
                }
        }
        
        //Agregar platillo nuevo
        @FXML
        private void agregarPla(ActionEvent event){
            Platillo platillo = new Platillo();
            dbPlatillo db = new dbPlatillo();
            tipoPlatillo tip = (tipoPlatillo) boxTipoPlatillo.getValue();
            
            platillo.setNombre(txtNombrePlatillo.getText());
            platillo.setPrecio(Double.parseDouble(txtPrecio.getText()));
            platillo.setDescripcion(txtDescripcion.getText());
            platillo.setDisponibilidad(boxDisponibilidad.getValue().toString());
            platillo.setTipo(tip.name());
            try {
                baseLXIV base = new baseLXIV();
                 BufferedImage imagen = ImageIO.read(this.filePla); 
                platillo.setImagen(base.encodeToString(imagen, "jpg"));
                
                db.insert(platillo);
                
                obtenerPlatillos(this.tipoPla);
                this.filePla = null;
                
                lblCodigoPla.setText(String.valueOf(CodigoActualPlatillo()));
            } catch (IOException ex) {
                Logger.getLogger(AdministracionController.class.getName()).log(Level.SEVERE, null, ex);
            }
                    
        }
        
        //Imprime los platillos en la tabla   
        private void obtenerPlatillos(String tipo){
            tablaPlatillo.getItems().clear();
            baseLXIV base = new baseLXIV();
            dbPlatillo db = new dbPlatillo();
           
            
            List<Platillo> lista = db.readSearch(tipo);
            
            Iterator<Platillo> i= lista.iterator();
            while(i.hasNext()){
                oblistP.add(i.next());
            }
             
            //frame.setImage(base.decodeToImage(empleado.getFoto()));
             columnIdP.setCellValueFactory(new PropertyValueFactory("id"));
             columnPlatillo.setCellValueFactory(new PropertyValueFactory("nombre"));
             columnPrecioP.setCellValueFactory(new PropertyValueFactory("precio"));
             columnDescripcionP.setCellValueFactory(new PropertyValueFactory("Descripcion"));
             columnTipoP.setCellValueFactory(new PropertyValueFactory("tipo"));
             columnDisponible.setCellValueFactory(new PropertyValueFactory("disponibilidad"));
             
             
             tablaPlatillo.setItems(oblistP);
        }
        
        //Para imprimir la imagen del platillo
        @FXML
        private void imprimeImgPla(MouseEvent event){
            baseLXIV base = new baseLXIV();
            Platillo platillo = tablaPlatillo.getSelectionModel().getSelectedItem();
            //System.out.println(platillo.getNombre());
            imagenPlatillo.setImage(base.decodeToImage(platillo.getImagen()));
        }
        
        //Desde los radioButton se evalua el seleccionado para realizar una busqueda
        @FXML
        private void buscarTipoPlatillo(ActionEvent event){
                
            if(buttonMar.isSelected()){
                this.tipoPla = "Mariscos";
            }else if (buttonBeb.isSelected()){
                this.tipoPla = "Bebidas";
            }else{
                this.tipoPla = "Japonesa";
            }
            
            obtenerPlatillos(this.tipoPla);
        }
        
        //Eliminar un platillo
        @FXML
        private void eliminarPla(ActionEvent event){
            dbPlatillo db = new dbPlatillo();
            Platillo platillo = tablaPlatillo.getSelectionModel().getSelectedItem();
            
            db.delete(platillo.getId());
            
            obtenerPlatillos(this.tipoPla);
        }
        
        //Subir la informacion del dato seleccionado
        @FXML
        private void upPlatillo(ActionEvent event){
            Platillo platillo = tablaPlatillo.getSelectionModel().getSelectedItem();
            
            lblCodigoPla.setText(String.valueOf(platillo.getId()));
            txtPrecio.setText(String.valueOf(platillo.getPrecio()));
            txtNombrePlatillo.setText(platillo.getNombre());
            txtDescripcion.setText(platillo.getDescripcion());
            boxTipoPlatillo.setValue(tipoPlatillo.valueOf(platillo.getTipo().replace(" ","")));
            boxDisponibilidad.setValue(platillo.getDisponibilidad().replace(" ",""));
            
        }
        
        //Modificar Platillo
        @FXML
        private void modificarPlatillo(ActionEvent event){
            Platillo platillo = new Platillo();
            dbPlatillo db = new dbPlatillo();
            tipoPlatillo tip = (tipoPlatillo) boxTipoPlatillo.getValue();
            
            platillo.setId(Integer.parseInt(lblCodigoPla.getText()));
            platillo.setNombre(txtNombrePlatillo.getText());
            platillo.setPrecio(Double.parseDouble(txtPrecio.getText()));
            platillo.setDescripcion(txtDescripcion.getText());
            platillo.setTipo(tip.name());
            platillo.setDisponibilidad(boxDisponibilidad.getValue().toString());
            
            if (!txtUrlPlatillo.getText().equals("")){
                try {
                    baseLXIV base = new baseLXIV();
                     BufferedImage imagen = ImageIO.read(this.filePla); 
                    platillo.setImagen(base.encodeToString(imagen, "jpg"));
                } catch (IOException ex) {
                    Logger.getLogger(AdministracionController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }else{
                platillo.setImagen("");
            }
            
            txtUrlPlatillo.setText("");
            this.filePla = null;
            
            db.update(platillo);
            obtenerPlatillos(this.tipoPla);
            
        }

    public void usuario() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}

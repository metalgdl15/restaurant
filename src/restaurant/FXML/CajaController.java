/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restaurant.FXML;

import DB.dbComanda;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import restaurant.Clases.Comanda;
import restaurant.Clases.Empleado;
import restaurant.Clases.Platillo;

/**
 * FXML Controller class
 *
 * @author gnr_a
 */
public class CajaController implements Initializable {
    
    private Empleado usuario;
    
    private static final double iva = 1.16;
    
    @FXML TextField txtFolio, txtCaja;
    @FXML Label lblimporte, lbltotal,nombreUsuario;
    
    @FXML TableView <Comanda> tablaComanda;
    @FXML TableColumn <Comanda, Integer> columnFolio;
    @FXML TableColumn <Comanda, String> columnMesa;
    @FXML TableColumn <Comanda, String> columnStatus;
    @FXML TableColumn <Comanda, String> columnFechaInicio;
    @FXML TableColumn <Comanda, String> columnFechaFin;
    @FXML TableColumn <Comanda, String> columnMesero;    
    ObservableList <Comanda> oblistCom= FXCollections.observableArrayList();
    
    @FXML TableView <Platillo> tablaPlatillos;
    @FXML TableColumn <Platillo, Integer> columnCantidad;
    @FXML TableColumn <Platillo, Double> columnPrecioUnitario;
    @FXML TableColumn <Platillo, Double> columnPrecioPlatillos;
    @FXML TableColumn <Platillo, String> columnPlatillo;
    
    ObservableList <Platillo> oblistPla= FXCollections.observableArrayList();
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        obtenerComandas();
    }
    
    public void usuario(Empleado empleado){
        this.usuario = empleado;
        nombreUsuario.setText(empleado.getNombre() + " " + empleado.getApellidoP());
    }
    public void obtenerComandas(){
        tablaComanda.getItems().clear();
        dbComanda db = new dbComanda();
       
        List<Comanda> lista = db.readComanda();
        
        Iterator <Comanda> i = lista.iterator();
        
        while(i.hasNext()){
            oblistCom.add(i.next());
        }
        
        columnFolio.setCellValueFactory(new PropertyValueFactory("folio"));
        columnMesero.setCellValueFactory(new PropertyValueFactory("mesero"));
        columnFechaInicio.setCellValueFactory(new PropertyValueFactory("fechaIngreso"));
        columnFechaFin.setCellValueFactory(new PropertyValueFactory("fechaSalida"));
        columnStatus.setCellValueFactory(new PropertyValueFactory("status"));
        columnMesa.setCellValueFactory(new PropertyValueFactory("mesa"));
        tablaComanda.setItems(oblistCom);
    }
    
    @FXML
    public void searchComanda(KeyEvent event){
        if (txtFolio.getText().length() > 0){
            
            tablaComanda.getItems().clear();
            dbComanda db = new dbComanda();


            List<Comanda> lista = db.searchComanda(txtFolio.getText());

            Iterator <Comanda> i = lista.iterator();

            while(i.hasNext()){
                oblistCom.add(i.next());
            }

            columnFolio.setCellValueFactory(new PropertyValueFactory("folio"));
            columnMesero.setCellValueFactory(new PropertyValueFactory("mesero"));
            columnFechaInicio.setCellValueFactory(new PropertyValueFactory("fechaIngreso"));
            columnFechaFin.setCellValueFactory(new PropertyValueFactory("fechaFin"));
            columnStatus.setCellValueFactory(new PropertyValueFactory("status"));
            columnMesa.setCellValueFactory(new PropertyValueFactory("mesa"));
            tablaComanda.setItems(oblistCom);
        }else{
            obtenerComandas();
        }
    }

    @FXML
    public void obtenerPlatillos(MouseEvent event){
        double Total = 0;
        double importe = 0;
        dbComanda db = new dbComanda();
        Comanda comanda = tablaComanda.getSelectionModel().getSelectedItem();
        
        tablaPlatillos.getItems().clear();
        
        List<Platillo> lista = db.readPlatillos(comanda.getFolio());
        
        
        for (Platillo platillo : lista) {
            oblistPla.add(platillo);
            //Sacamos el importe
            importe+=platillo.getCantidad()*platillo.getPrecio();    
        }
        
        Total = importe * iva;
        
        //Hasta aqui imprimimos los datos en la tabla
        
        columnCantidad.setCellValueFactory(new PropertyValueFactory("cantidad"));
        columnPlatillo.setCellValueFactory(new PropertyValueFactory("nombre"));
        columnPrecioUnitario.setCellValueFactory(new PropertyValueFactory("precio"));
        columnPrecioPlatillos.setCellValueFactory(new PropertyValueFactory("total"));

        tablaPlatillos.setItems(oblistPla);
        
        DecimalFormat df = new DecimalFormat("#.##");
        
        lblimporte.setText(String.valueOf(importe));
        lbltotal.setText(String.valueOf(df.format(Total)));
            
    }
    
    @FXML
    public void finalizarComanda(ActionEvent event){
        dbComanda db = new dbComanda();
        
        Comanda comanda = tablaComanda.getSelectionModel().getSelectedItem();
        String caja = txtCaja.getText();
        
        db.updateCajero(caja, usuario.getCodigo());
        
    }
    
}

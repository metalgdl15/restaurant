/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restaurant;

import DB.dbUsuario;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import restaurant.Clases.Empleado;
import restaurant.FXML.AdministracionController;
import restaurant.FXML.CajaController;
import restaurant.FXML.CocinaController;
import restaurant.FXML.ComandaController;

/**
 *
 * @author gnr_a
 */
public class FXMLDocumentController implements Initializable {
  
    @FXML TextField usuario;
    @FXML PasswordField password;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    @FXML private void verificarUsuario(ActionEvent event) throws IOException{
        dbUsuario db = new dbUsuario();
        
        int user = Integer.parseInt(this.usuario.getText());
        String contra = password.getText();
                
        Empleado empleado = db.Verificar(user, contra);
        
        Stage stage = new Stage();
        FXMLLoader load = new FXMLLoader();
        AnchorPane root = null;
        
        String puesto = empleado.getPuesto().replace(" ", "");
        System.out.println(puesto);
        
        
        switch(puesto){
            case "Administrador":
                root = (AnchorPane)load.load(getClass().getResource("FXML/Administracion.fxml").openStream());                  
                AdministracionController admin = (AdministracionController) load.getController();
                admin.usuario(empleado);
                break;
            case "Mesero": 
                root = load.load(getClass().getResource("FXML/Comanda.fxml").openStream());             
                ComandaController comanda= (ComandaController) load.getController();
                comanda.usuario(empleado);
                break;
                
            case "Caja":  
                root =(AnchorPane)load.load(getClass().getResource("FXML/Caja.fxml").openStream());
                CajaController caja = (CajaController) load.getController();
                caja.usuario(empleado);
                break;
            case "Cocinero":  
                root = (AnchorPane)load.load(getClass().getResource("FXML/Cocina.fxml").openStream());
                CocinaController cocina = (CocinaController) load.getController();
                cocina.usuario(empleado);
                break;                                   
        }
       
        stage.setScene(new Scene(root));
        stage.show();
        
       
        ((Node)(event.getSource())).getScene().getWindow().hide(); 
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restaurant.Clases;

import java.util.List;

/**
 *
 * @author gnr_a
 */
public class Comanda {
    private final static double iva = 0.16;
    private int folio;
    private String fechaIngreso;
    private String fechaSalida;
    private Empleado mesero;
    private Empleado cocinero;
    private String cliente;
    private String status;
    private Empleado cajero;
    private String caja;
    private List<Platillo> platillos;
    private int platillo;
    private double Total;
    private double importe;
    private String mesa;

    /**
     * @return the folio
     */
    public int getFolio() {
        return folio;
    }

    /**
     * @param folio the folio to set
     */
    public void setFolio(int folio) {
        this.folio = folio;
    }

    /**
     * @return the empleado
     */
    public Empleado getMesero() {
        return mesero;
    }

    /**
     * @param mesero the empleado to set
     */
    public void setMesero(Empleado mesero) {
        this.mesero = mesero;
    }

    /**
     * @return the cocinero
     */
    public Empleado getCocinero() {
        return cocinero;
    }

    /**
     * @param cocinero the cocinero to set
     */
    public void setCocinero(Empleado cocinero) {
        this.cocinero = cocinero;
    }

    /**
     * @return the cliente
     */
    public String getCliente() {
        return cliente;
    }

    /**
     * @param cliente the cliente to set
     */
    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the cajero
     */
    public Empleado getCajero() {
        return cajero;
    }

    /**
     * @param cajero the cajero to set
     */
    public void setCajero(Empleado cajero) {
        this.cajero = cajero;
    }

    /**
     * @return the caja
     */
    public String getCaja() {
        return caja;
    }

    /**
     * @param caja the caja to set
     */
    public void setCaja(String caja) {
        this.caja = caja;
    }

    /**
     * @return the platillos
     */
    public List<Platillo> getPlatillos() {
        return platillos;
    }

    /**
     * @param platillos the platillos to set
     */
    public void setPlatillos(List<Platillo> platillos) {
        this.platillos = platillos;
    }

    /**
     * @return the platillo
     */
    public int getPlatillo() {
        return platillo;
    }

    /**
     * @param platillo the platillo to set
     */
    public void setPlatillo(int platillo) {
        this.platillo = platillo;
    }

    /**
     * @return the Total
     */
    public double getTotal() {
        return Total;
    }

    /**
     * @param Total the Total to set
     */
    public void setTotal(double Total) {
        this.Total = Total;
    }

    /**
     * @return the importe
     */
    public double getImporte() {
        return importe;
    }

    /**
     * @param importe the importe to set
     */
    public void setImporte(double importe) {
        this.importe = importe;
    }

    /**
     * @return the mesa
     */
    public String getMesa() {
        return mesa;
    }

    /**
     * @param mesa the mesa to set
     */
    public void setMesa(String mesa) {
        this.mesa = mesa;
    }

    /**
     * @return the fechaIngreso
     */
    public String getFechaIngreso() {
        return fechaIngreso;
    }

    /**
     * @param fechaIngreso the fechaIngreso to set
     */
    public void setFechaIngreso(String fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    /**
     * @return the fechaSalida
     */
    public String getFechaSalida() {
        return fechaSalida;
    }

    /**
     * @param fechaSalida the fechaSalida to set
     */
    public void setFechaSalida(String fechaSalida) {
        this.fechaSalida = fechaSalida;
    }
    
}
